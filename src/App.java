import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class App {

	public static void main(String[] args) {
		
		try {
			File file = new File("te.svg");
			BufferedWriter fr = new BufferedWriter(new FileWriter(file));
			Scanner s = new Scanner(new File("teste.txt"));
			ArrayList<String> maze = new ArrayList<>();
			int altura = s.nextInt();
			int largura = s.nextInt();
			
			while (s.hasNext()) {
				maze.add(hexToBinary(s.next()));
			}
			
			fr.write("<?xml version=\"1.0\" standalone=\"no\"?>\n");
			fr.write("\t<svg xmlns=\"http://www.w3.org/2000/svg\" width=\""+ altura + "cm\" height=\"" + largura + "cm\" viewBox=\"-0.1 -0.1 " + altura + ".2 " + largura + ".2\">\n");
			fr.write("\t\t<g style=\"stroke-width:.1; stroke:black; stroke-linejoin:miter; stroke-linecap:butt;\">\n");

			int l = 0;
			int c = 0;
			
			for(int i = 0; i < maze.size(); i++) {
				
				if (maze.get(i).charAt(0) == '1') {
					//norte
					escreveLinha(c, l, c+1, l, fr);
				} 
				if (maze.get(i).charAt(1) == '1') {
					//leste
					escreveLinha(c+1, l, c+1, l+1, fr);
				} 
				if (maze.get(i).charAt(2) == '1') {
					//sul
					escreveLinha(c, l+1, c+1, l+1, fr);
				} 
				if (maze.get(i).charAt(3) == '1') {
					//oeste
					escreveLinha(c, l, c, l+1, fr);
				}
				
				
				c++;
				if (c == largura) {
					c = 0;
					l++;
				}
			}
			
			fr.write("</g>");
			fr.write("</svg>");
			
			fr.close();
			
		} catch (FileNotFoundException e) {
			
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
	
	public static void escreveLinha(int x1, int y1, int x2, int y2, BufferedWriter fr) throws IOException {
		fr.write("\t\t\t<polyline points=\"" + x1 + "," + y1 + " "  + x2 + "," + y2 + "\" />\n");
	}
	
	public static String hexToBinary(String hex) {
		int i = Integer.parseInt(hex, 16);
		String bin = Integer.toBinaryString(i);
		while (bin.length() < 4) {
			bin = "0" + bin;
		}
		return bin;
	}

}
